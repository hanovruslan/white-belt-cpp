#include <iostream>

using namespace std;

const uint32_t MIN = 1;
const uint32_t MAX = 10;

int main() {
    uint32_t param, result = 1;
    cout << "Please, enter a number between " << MIN <<  " and " << MAX << endl;
    cin >> param;
    if (param > MIN && param <= MAX) {
        for (uint32_t i = MIN + 1; i <= param; i++) {
            result *= i;
        }
    }
    cout << "Result is " << result << endl;

    return 0;
}
