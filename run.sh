#!/usr/bin/env bash

std="c++14"
o="main.out"
i="main.cpp"

g++ ${i} -o ${o} -std=${std} && \
./main
